﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(My_CodeFirst_MVC.Startup))]
namespace My_CodeFirst_MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
