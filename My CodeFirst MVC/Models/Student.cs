﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace My_CodeFirst_MVC.Models
{
  
    public class Student
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set;}
        public virtual ICollection<Enrolllment> Enrolllments { get; set; }


    }
}