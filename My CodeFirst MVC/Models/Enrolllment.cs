﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace My_CodeFirst_MVC.Models
{
    public class Enrolllment
    {
        public int EnrolllmentID {get;set;}
        public int StudentID { get; set; }
        public int CourseID { get; set; }
        public virtual Student Student { get; set; }
        public virtual Course Course { get; set; }
    }
}